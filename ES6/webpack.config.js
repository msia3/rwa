const path = require('path');

module.exports = {
    entry: {
        app: ['./src/index.js'],
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.js",
    },
    devServer: {
        publicPath: "/dist/",  //<- this defines URL component #2 above,
    },
    devtool: "source-map",
    watch: true,
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
    },
    plugins: [
        /*
        new browserPlugin({
            openOptions: {
                app: [
                    'google chrome',
                    //'--incognito',
                    '--disable-web-security', // to enable CORS
                    '--user-data-dir' // to let Chrome create and store here developers plugins, settings, etc.
                ]
            }
        })
        */
    ],   
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2017']
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            }
        ]
    }
}