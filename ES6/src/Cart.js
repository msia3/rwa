import * as Rxjs from 'rxjs';
import { KursnaListaService } from './services/kursnaLista.service';
import { MotherboardsService } from './services/motherboards.service';
import { CPUsService } from './services/cpus.service';
import { MemoryService } from './services/memory.service';
import { GPUService } from './services/gpus.service';

import { KursnaLista } from './services/kursnaLista.service';
import { CPU } from './components/cpu';
import { Motherboard } from './components/motherboard';
import { Formatter } from './helpers/formatter';

import notify from 'izitoast';
import { StorageService } from './services/storage.service';

export class Cart {
    /**
     * @param {string} name
     * @param {element} body 
     */
    constructor(name, body) {
        this.name = name;

        this.cart = [];

        this.formatter = new Formatter();
        this.kursnaLista = new KursnaLista('ad4195ec9396867b7a234ff1d29fe0b9');

        this.services = [
            new MotherboardsService('https://png.icons8.com/dusk/50/000000/motherboard.png', this.kursnaLista),
            new CPUsService('https://png.icons8.com/dusk/50/000000/processor.png', this.kursnaLista),
            new MemoryService('https://png.icons8.com/dusk/50/000000/memory-slot.png', this.kursnaLista),
            new GPUService('https://png.icons8.com/dusk/50/000000/network-card.png', this.kursnaLista),
            new StorageService('https://png.icons8.com/dusk/50/000000/ssd.png', 'SSD', 'SSD', this.kursnaLista),
            new StorageService('https://png.icons8.com/dusk/50/000000/hdd.png', 'HDD', 'HDD', this.kursnaLista)
        ];

        this._render(body);


        this.kursnaLista.init()
        .then(() => {
            //Trigger click event for MOBO
            document.getElementsByTagName('button')[0].click();

        });
    }

    _totalCost(){
        return new Promise((resolve, reject) => {
            const total = this.cart
                    .map(val => val.price * val.quantity)
                    .reduce((total, acc) => total + acc);
                    
            total === 0 ? reject() : resolve(total);
        })
        
    }

    _addItemToCart(item){
        let index = this.cart.findIndex(e => e instanceof item.constructor && e.name == item.name);
        if(index != -1){
            this.cart[index].quantity++;

            let el = document.getElementById('item-' + (index+1));
            el.querySelector('.quantity').innerHTML = this.cart[index].quantity;
        } else {
            this.cart.push(item);

            let orders = document.getElementById('cart-list');
            //Item
            let order = document.createElement('li');
            order.id = 'item-' + this.cart.length;
            order.className = 'list-group-item lh-condensed';
            orders.insertBefore(order, orders.firstChild);

            item.renderCart(order)
                .then((button) => {
                    Rxjs.Observable.fromEvent(button, 'click')
                        .subscribe(() => {
                            let item = button.ref;
                            let index = this.cart.findIndex((e, index) => e instanceof item.constructor);

                            var el = button.parentElement;
                            while(el.tagName != 'LI'){
                                el = el.parentElement;
                            }

                            if((--this.cart[index].quantity) > 0){
                                el.querySelector('.quantity').innerHTML = this.cart[index].quantity;
                            } else {
                                this.cart.splice(index, 1);
                                el.parentNode.removeChild(el);
                            }
                            
                            this._renderItemNum();
                            this._renderCost();
                        });
                });;
            item.quantity++;
        }

        this._renderCost();
        this._renderItemNum();
    }

    _renderItemNum(){
        let el = document.getElementsByClassName('items-count')[0];
        el.innerHTML = this.cart
                        .map(e => e.quantity)
                        .reduce((total, acc) => total + acc, 0);
    }

    _renderCost(){
        let totalEl = document.getElementById('total');

        this._totalCost()
            .then((totalCost) => totalEl.innerHTML = this.formatter.ispisi(totalCost))
            .catch(() => totalEl.innerHTML = '--');
    }

    _render(body){
        //log('Rendering template...');

        //Header
        let div = document.createElement('div');
        div.className = 'px-3 py-3 pt-md-5 pb-md-5 mx-auto text-center';
        let h1 = document.createElement('h1');
        h1.className = 'display-4';
        h1.innerHTML = this.name;
        div.appendChild(h1);

        body.appendChild(div);

        //Nav row
        div = document.createElement('div');
        div.className = 'row mb-4';

        let col = document.createElement('div');
        col.className = 'col-md-8';

        let toolbar = document.createElement('div');
        toolbar.className = 'btn-toolbar';
        
        col.appendChild(toolbar);
        
        //Dugmici za navigaciju
        this.services.forEach((service) => {
            let button = document.createElement('button');
            button.className = 'btn btn-lg btn-outline-primary mr-2';
            button.type = 'button';
            button.setAttribute('data-toggle', 'tooltip');
            button.setAttribute('title', service.name);

            let img = document.createElement('img');
            img.src = service.icon;

            button.appendChild(img);
            toolbar.appendChild(button);

            Rxjs.Observable.fromEvent(button, 'click')
                .exhaustMap(() => this._loadService(service))
                .subscribe(() => {
                    service
                        .render(document.getElementById('component-list'))
                        .then(() => {

                            let items = document.getElementsByClassName('add-item');

                            Rxjs.Observable.fromEvent(items, 'click')
                                .map((ev) => ev.currentTarget.ref)
                                .subscribe((item) => {
                                    service
                                        .check(this.cart, item)
                                        .then(() => {
                                            this._addItemToCart(item)
                                        })
                                        .catch((error) => notify.error({
                                            'title': 'Greška',
                                            'message': error,
                                        }));
                                });
                        })
                });
        });

        div.appendChild(col);
        body.appendChild(div);

        // Sledeci red
        // 8 kolona za prikaz komponenti
        // 4 kolone za prikaz finalnog racuna

        div = document.createElement('div');
        div.className = 'row mb-4';

        let col8 = document.createElement('div');
        col8.className = 'col-md-8';
        //col8.id = 'component-list';

        //Spinner element
        let spinner = document.createElement('div');
        spinner.className = 'row align-items-center text-center h-100 d-none';
        spinner.id = 'components-spinner';
        col8.appendChild(spinner);

        let spinner_col = document.createElement('div');
        spinner_col.className = 'col mx-auto';
        spinner.appendChild(spinner_col);

        let icon = document.createElement('i');
        icon.className = 'fas fa-spinner fa-spin fa-5x';
        spinner_col.appendChild(icon);

        //Attach list body
        let ul = document.createElement('ul');
        ul.className = 'list-group';
        ul.id = 'component-list';
        col8.appendChild(ul);

        div.appendChild(col8);

        let col4 = document.createElement('div');
        col4.className = 'col-md-4 order-md-2 mb-4';
        div.appendChild(col4);

        let h4 = document.createElement('h4');
        h4.className = 'd-flex justify-content-between align-items-center mb-3';

        let span = document.createElement('span');
        span.className = 'text-muted';
        span.innerHTML = 'Vaša konfiguracija';
        h4.appendChild(span);

        span = document.createElement('span');
        span.className = 'badge badge-secondary badge-pill items-count';
        span.innerHTML = 0;

        h4.appendChild(span);

        col4.appendChild(h4);


        //Lista narudzbina
        let orders = document.createElement('ul');
        orders.className = 'list-group mb-3';
        orders.id = 'cart-list';
        col4.appendChild(orders);

        /*
        for(let i=0; i< 4; i++){
            let order = document.createElement('li');
            order.className = 'list-group-item d-flex justify-content-between lh-condensed';
            let div = document.createElement('div');
            order.appendChild(div);

            let h6 = document.createElement('h6');
            h6.className = 'my-0';
            h6.innerHTML = 'Neki test';

            div.appendChild(h6);

            span = document.createElement('span');
            span.className = 'text-muted';
            span.innerHTML = '12$';

            order.appendChild(span);

            orders.appendChild(order);
        }
        */

        let li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between';
        let total_span = document.createElement('span');
        total_span.innerHTML = 'Ukupno:';
        li.appendChild(total_span);
        let strong = document.createElement('strong');
        strong.id = 'total';
        strong.innerHTML = '--'
        li.appendChild(strong);

        orders.appendChild(li);

        body.appendChild(div);
    }

    _loadService(service){
        this._showLoader();
        return service.get(this.cart)
            .then(() => this._hideLoader());
    }

    _showLoader(){
        let list = document.getElementById('component-list');
        list.classList.add('d-none');
        list.classList.remove('d-flex');

        let spinner = document.getElementById('components-spinner');
        spinner.classList.add('d-flex');
        spinner.classList.remove('d-none');
    }

    _hideLoader(){
        let spinner = document.getElementById('components-spinner');
        spinner.classList.remove('d-flex');
        spinner.classList.add('d-none')

        let list = document.getElementById('component-list');
        list.classList.remove('d-none');
        list.classList.add('d-flex');
    }
}