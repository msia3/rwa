/*
const DEBUG = true;

function log(text){
    if(DEBUG)
        console.log(text);
}
*/

import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import fontawesome from '@fortawesome/fontawesome';
import solid from '@fortawesome/fontawesome-free-solid';

import { Cart } from './Cart';

fontawesome.library.add(solid.faSearch);

window.onload = () => {
    console.log('Loading App...');
    const cart = new Cart('PC Konfigurator', document.getElementById('app'));

    //Bootstrap Tooltip
    $('[data-toggle="tooltip"]').tooltip();
}

/*
import { KursnaLista } from './services/kursnaLista.service';
import { MotherboardsService } from './services/motherboards.service';
*/



/*
const lista = new KursnaLista('ad4195ec9396867b7a234ff1d29fe0b9');
lista.init()
.then(() => {
    lista.konvertuj(20)
    .then((res) => console.log(res))
    .then(() => lista.konvertuj(10))
    .then(() => lista.konvertuj(30));
});
*/

//console.log(lista.konvertuj(20));

/*
const ms = new MotherboardsService();
ms.get()
.then((mobos) => console.log(mobos));
*/