import { CPU } from '../components/cpu';
import { Motherboard } from '../components/motherboard';
import { RAM } from '../components/memory';

const pcpartpicker = require('pcpartpicker');

export class MemoryService {

    constructor(icon, kursnaLista){
        this.name = 'Memorija';
        this.rams = [];
        this.icon = icon;

        this.kursnaLista = kursnaLista;
    }

    get(items) {
        return new Promise((resolve) => {

            /*
            if(this.cpus.length > 0){
                console.log('Getting CPUs from local storage');                
                return resolve(this.cpus);
            }
            */
            console.log('Getting RAM from API');

            let sort = {};

            let mobos = items.filter(e => e instanceof Motherboard);
            if(mobos.length > 0 ){
                sort.price = '+';
                
                let per = parseInt(mobos[0].maxRam/mobos[0].ramSlots);
                sort.size = [mobos[0].maxRam + "GB (" + mobos[0].ramSlots + "x" + per + "GB)"];
            }

            pcpartpicker.getMemory(sort,(rams) => {
                this.rams = rams
                .map((ram) => new RAM(
                    ram.name, this.kursnaLista.konvertuj(ram.price), ram.speed, ram.type, ram.cas, ram.modules, ram.size, ram.ppg, ram.url
                ));
                resolve(this.rams);
            }, 1);
        });
    }

    check(items, item){
        return new Promise((resolve, reject) => {
            if(items.some(e => e instanceof RAM))
                reject('Već ste dodali memoriju!');

            //if(items.some(e => e instanceof Motherboard).filter(e => e.socket != item.socket).length > 0)
                //reject('Socket procesora se ne poklapa sa matičnom pločom!');
            //if(items.some(e => e instanceof RAM))

            resolve();
        });
    }

    render(body){
        console.log('Memory Render');

        return new Promise((resolve) => {
            body.innerHTML = '';
            this.rams.forEach((ram) => {
                ram._renderElement(body);
            });

            resolve();
        });
    }
}