import { Storage } from '../components/storage';

const pcpartpicker = require('pcpartpicker');

export class StorageService {

    constructor(icon, name, type = 'HDD', kursnaLista){
        this.name = name;
        this.storage = [];
        this.icon = icon;
        this.type = type;

        this.kursnaLista = kursnaLista;
    }

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting Storage from API');

            let sort = {};
            sort.type = this.type;
            
            pcpartpicker.getStorage(sort,(storage) => {
                this.storage = storage
                .map((item) => new Storage(
                    item.name, this.kursnaLista.konvertuj(item.price), item.series, item.form, item.type, item.capacityArray, item.cache, item.pricePerGB, item.url
                ));
                resolve(this.storage);
            }, 1);
        });
    }

    check(items, item){
        return new Promise((resolve, reject) => {
            let num = items
                .filter(e => e instanceof Storage)
                .map(e => e.quantity)
                .reduce((total, acc) => total + acc, 0);
            if(num > 3)
                reject('Ne možete dodati više od 4 diska!');

            //if(items.some(e => e instanceof CPU).filter(e => e.socket != item.socket).length > 0)
                //reject('Socket procesora se ne poklapa sa matičnom pločom!');
            //if(items.some(e => e instanceof RAM))

            resolve();
        });
    }

    render(body){
        console.log('Storage Render');

        return new Promise((resolve) => {
            body.innerHTML = '';
            this.storage.forEach((item) => {
                item._renderElement(body);
            });

            resolve();
        });
    }
}