import { Formatter } from '../helpers/formatter';
export class Motherboard {

    constructor(name, socket, price, formFactor, ramSlots, maxRam, url){
        this.name = name;
        this.socket = socket;
        this.price = parseFloat(price);
        this.formFactor = formFactor;
        this.ramSlots = ramSlots;
        this.maxRam = maxRam;
        this.url = url;

        this.quantity = 0;

        this.formatter = new Formatter();
    }

    renderCart(body){
        return new Promise((resolve, reject) => {
            body.innerHTML = `
                <div class="row justify-content-between">
                    <div class="col-10">
                        <span> ${this.name} </span>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-outline-danger btn-sm remove-item"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <small>
                            <span> <span class="font-weight-bold">Socket:</span> ${this.socket}</span>
                            <span> <span class="font-weight-bold">RAM slotovi:</span> ${this.ramSlots}</span>
                            <span> <span class="font-weight-bold">Max RAM:</span> ${this.maxRam} GB</span>
                        </small>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-6">
                        <span class="badge badge-primary">${this.formatter.ispisi(this.price)}</span>
                    </div>
                    <div class="col-6 text-right">
                        <span>Količina:</span>
                        <span class="quantity">1</span>
                    </div>
                </div>`;

            let button = body.getElementsByTagName('button')[0];
            button.ref = this;

            resolve(button);
        })
    }

    _renderElement(body){
        let el = document.createElement('li');
        el.className = 'list-group-item';
        body.appendChild(el);

        el.innerHTML = `
        <div class="row align-items-center h-100">
            <div class="col-1 max-auto text-center">
                <a href=${this.url} target="_blank"><i class="fas fa-search fa-sm mr-2"></i></a>
            </div>
            <div class="col-6 pl-0">
                <div class="row">
                    <div class="col">
                        <span> ${this.name} </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <small>
                            <span> <span class="font-weight-bold">Socket:</span> ${this.socket}</span>
                            <span> <span class="font-weight-bold">RAM slotovi:</span> ${this.ramSlots}</span>
                            <span> <span class="font-weight-bold">Max RAM:</span> ${this.maxRam} GB</span>
                        </small>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <span class="badge badge-primary">${this.formatter.ispisi(this.price)}</span>
            </div>
            <div class="col-2">
                <button type="button" class="btn btn-outline-success add-item"><i class="fas fa-plus"></i></button>
            </div>
        </div>`;

        let button = el.getElementsByTagName('button')[0];
        button.ref = this;
    }
}