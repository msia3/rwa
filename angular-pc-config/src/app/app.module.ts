import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { rootReducer } from './store/reducers';

import {EffectsModule} from '@ngrx/effects';
import {effects} from './store/effects';

import { Formatter } from './helpers/formatter';

import {KursnaLista} from './services/kursnaLista.service';
import {MotherboardsService} from './services/motherboard.service';
import {CPUsService} from './services/cpus.service';
import {MemoryService} from './services/memory.service';
import {GPUService} from './services/gpus.service';
import { StorageService } from './services/storage.service';


import { ListItemComponent } from './components/list-item/list-item.component';
import { CartComponent } from './components/cart/cart.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ListItemComponent,
    CartComponent,
    CartItemComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({})
  ],
  providers: [
    Formatter,
    KursnaLista,
    MotherboardsService,
    CPUsService,
    MemoryService,
    GPUService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
