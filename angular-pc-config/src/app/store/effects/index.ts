import { ItemsEffects } from './items.effect';
import { CartEffects } from './cart.effect';

export const effects:any [] = [ItemsEffects, CartEffects];

export * from './items.effect';
export * from './cart.effect';
