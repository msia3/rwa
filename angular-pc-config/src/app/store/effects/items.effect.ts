import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { switchMap, map, take } from 'rxjs/operators';

import * as itemActions from '../actions';
import { State } from '../reducers';
import { Component as Comp } from '../../models/components';
import { getCartItems } from '../reducers/cart.reducer';

import { MotherboardsService } from '../../services/motherboard.service';
import { CPUsService } from '../../services/cpus.service';
import { MemoryService } from '../../services/memory.service';
import { GPUService } from '../../services/gpus.service';
import { StorageService } from '../../services/storage.service';

@Injectable()
export class ItemsEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<State>,
    private moboService: MotherboardsService,
    private cpuService: CPUsService,
    private memoryService: MemoryService,
    private gpuService: GPUService,
    private storageService: StorageService
  ){
  }

  @Effect()
  loadData$ = this.actions$.ofType<itemActions.requestAPI>(itemActions.REQUEST_API_DATA)
  .pipe(
    map(type => type.payload),
    switchMap((type) => {
      let service;
      switch(type){
        case 'cpu':
          service = this.cpuService;
        break;
        case 'mobo':
          service = this.moboService;
        break;
        case 'memory':
          service = this.memoryService;
        break;
        case 'gpu':
          service = this.gpuService
        break;
        case 'ssd': {
          service = this.storageService;
          service.setType('SSD');
        }
        break;
        case 'hdd': {
          service = this.storageService;
          service.setType('HDD');
        }
        break;
      }

      return this.store$.pipe(take(1), select(getCartItems), switchMap((components: Comp[], i:number) => {
        return service.get(components).then((items) => {
          return new itemActions.receiveAPI(items);
        });
      }));
    })
  );
}
