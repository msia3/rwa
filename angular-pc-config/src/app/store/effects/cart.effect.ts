import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { switchMap, map, take, filter } from 'rxjs/operators';

import * as itemActions from '../actions';
import { State } from '../reducers';
import { getCPUsocket } from '../../services/pcpartpicker';
import { CPU } from '../../models/components';

@Injectable()
export class CartEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<State>
  ){
  }

  @Effect()
  loadData$ = this.actions$.ofType<itemActions.addItem>(itemActions.ADD_ITEM)
  .pipe(
    map(item => item.payload),
    filter(item => item instanceof CPU),
    switchMap((item) => {
      console.log(item);
      return getCPUsocket(item.url)
      .then((sock) => {
        item.socket = sock;
        return new itemActions.changeSocket({id: item.id, item: item});
      })
      .catch(() => console.log('CPU socket crawler error!'))
    })
  );
}
