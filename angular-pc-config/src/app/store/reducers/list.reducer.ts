import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Component } from '../../models/components';
import { configuratorActions, REQUEST_API_DATA, RECEIVE_API_DATA } from '../../store/actions';

//Interface stanja
export interface ListState extends EntityState<Component> {
  loading: boolean;
}

export const adapter : EntityAdapter<Component> = createEntityAdapter<Component>({});
export const getListState = createFeatureSelector<ListState>('list');

//Inicijalno stanje
export const initState: ListState = adapter.getInitialState({
  loading: false
});

export function listReducer(state = initState, action: configuratorActions): ListState {
  switch (action.type) {
    case REQUEST_API_DATA:
      return {
        ...state,
        loading: true
      };
    case RECEIVE_API_DATA:
    return adapter.addAll(action.payload, {...state, loading: false});
    default:
      return {...state, loading: false};
  }
}

export const {
  selectAll,
  selectEntities: getListEntities,
  selectIds: getListIds,
  selectTotal

} = adapter.getSelectors(getListState);

export const getItemsList = createSelector(
  getListEntities,
  getListIds,
  (entities, ids) => {
    return (ids as number[]).map(id => entities[id]);
  }
);

export const getItemsLoading = createSelector(
  getListState,
  (state: ListState) => state.loading
);

