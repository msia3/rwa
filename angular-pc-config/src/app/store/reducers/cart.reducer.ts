import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Component, CPU } from '../../models/components';
import { configuratorActions, ADD_ITEM, REMOVE_ITEM, CHANGE_SOCKET } from '../../store/actions';

//Interface stanja
export interface CartState extends EntityState<Component> {
  quantity: number,
  cost: number
}

export const adapter : EntityAdapter<Component> = createEntityAdapter<Component>({});
export const getCartState = createFeatureSelector<CartState>('cart');

//Inicijalno stanje
export const initState: CartState = adapter.getInitialState({
  quantity: 0,
  cost: 0
});

export function cartReducer(state = initState, action: configuratorActions): CartState {
  switch (action.type) {
    case ADD_ITEM: {
      let item = Object.values(state.entities).find((e, index) => e.name == action.payload.name);

      if(item != undefined){
        item.quantity = item.quantity + 1;
        return {
          ...state,
          quantity: state.quantity + 1,
          cost: state.cost + parseFloat(action.payload.price),
          entities: {
            ...state.entities,
            [item.id]: item
          }
        }
      } else {
        return adapter.addOne(action.payload, {
          ...state,
          quantity: state.quantity + 1,
          cost: state.cost + parseFloat(action.payload.price)
        });
      }
    }
    case REMOVE_ITEM: {
      let item = Object.values(state.entities).find((e, index) => e.name == action.payload.name);

      if(item == undefined) return {...state};

      if(item.quantity == 1) {
        return adapter.removeOne(action.payload.id, {
          ...state,
          quantity: state.quantity -1,
          cost: state.cost - parseFloat(action.payload.price)
        });
      } else {
        item.quantity = item.quantity - 1;
        return {
          ...state,
          quantity: state.quantity - 1,
          cost: state.cost - parseFloat(action.payload.price),
          entities: {
            ...state.entities,
            [item.id]: item
          }
        }
      }
    }
    case CHANGE_SOCKET:
      //radi samo sa serializable objektima
      //return adapter.updateOne(action.payload, state);
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.payload.id]: action.payload.item
        }
      }
    default:
      return {
        ...state
      }
  }
}

export const {
  selectAll,
  selectEntities: getListEntities,
  selectIds: getListIds,
  selectTotal

} = adapter.getSelectors(getCartState);

export const getCartItems = createSelector(
  getListEntities,
  getListIds,
  (entities, ids) => {
    return (ids as number[]).map(id => entities[id]);
  }
);

export const getCartQuantity = createSelector(
  getCartState,
  (state: CartState) => state.quantity
);

export const getCartCost = createSelector(
  getCartState,
  (state: CartState) => state.cost
);

