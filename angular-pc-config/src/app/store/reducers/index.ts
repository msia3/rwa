import { ActionReducerMap, createSelector } from '@ngrx/store';
import { ListState, listReducer } from './list.reducer';
import { CartState, cartReducer } from './cart.reducer';

export interface State {
  list: ListState,
  cart: CartState
}

export const rootReducer: ActionReducerMap<State> = {
  list: listReducer,
  cart: cartReducer
}
