import { Action } from '@ngrx/store';
import { CPU } from '../../models/components';

export const REQUEST_API_DATA = 'REQUEST_API_DATA';
export const RECEIVE_API_DATA = 'RECEIVE_API_DATA';
export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const CHANGE_SOCKET = 'CHANGE_SOCKET';

export class requestAPI implements Action {
    readonly type = REQUEST_API_DATA;

    constructor(public payload:string){}
}

export class receiveAPI implements Action {
    readonly type = RECEIVE_API_DATA;

    constructor(public payload:any){}
}

export class addToCart implements Action {
  readonly type = ADD_TO_CART;
}

export class addItem implements Action {
  readonly type = ADD_ITEM;

  constructor(public payload:any){}
}

export class removeItem implements Action {
  readonly type = REMOVE_ITEM;

  constructor(public payload:any){}
}

export class changeSocket implements Action {
  readonly type = CHANGE_SOCKET;

  constructor(public payload: {id: string, item: CPU}){}
}

export type configuratorActions =
  requestAPI |
  receiveAPI |
  addItem |
  removeItem |
  changeSocket;
