import { getCPUsocket } from '../services/pcpartpicker';

export class Component {

    constructor(
        public id: number,
        public name: string,
        public price: number,
        public quantity: number = 1
    ){}

    check(items){
      return null;
    }
}

export class Motherboard extends Component {
    constructor(
        public id: number,
        public name: string,
        public socket: string,
        public price: number = 0,
        public formFactor: string,
        public ramSlots: number,
        public maxRam: number,
        public url: string,
        public quantity: number = 1
    ) {
        super(
            id,
            name,
            price
        );
    }

    render(){
      return `
          <span> <strong>Socket:</strong> ${this.socket}</span>
          <span> <strong>RAM slotovi:</strong> ${this.ramSlots}</span>
          <span> <strong>Max RAM:</strong> ${this.maxRam} GB</span>
      `;
    }

    renderCart(){
      return this.render();
    }

    check(items){
      return new Promise((resolve, reject) => {
          if(items.some(e => e instanceof Motherboard))
              reject('Već ste dodali matičnu ploču!');

          //if(items.some(e => e instanceof CPU).filter(e => e.socket != item.socket).length > 0)
              //reject('Socket procesora se ne poklapa sa matičnom pločom!');
          //if(items.some(e => e instanceof RAM))

          resolve();
      });
    }
}

export class CPU extends Component {
  constructor(
      public id: number,
      public name: string,
      public price: number,
      public speed: string,
      public tdp: string,
      public cores: string,
      public url: string,
      public socket: string = null,
      public quantity: number = 1
  ) {
      super(
          id,
          name,
          price
      );
  }

  renderCart(){
      return this.render();
  }

  render(){
    return `
        <span> <strong>Brzina:</strong> ${this.speed} GHz</span>
        <span> <strong>Snaga:</strong> ${this.tdp} W</span>
        <span> <strong>Jezgra:</strong> ${this.cores} GB</span>
    `;
  }

  check(items){
    return new Promise((resolve, reject) => {
        if(items.some(e => e instanceof CPU))
            reject('Već ste dodali procesor!');

        //if(items.some(e => e instanceof Motherboard).filter(e => e.socket != item.socket).length > 0)
            //reject('Socket procesora se ne poklapa sa matičnom pločom!');
        //if(items.some(e => e instanceof RAM))

        resolve();
    });
  }
}

export class GPU extends Component {
  constructor(
      public id: number,
      public name: string,
      public price: number,
      public series: string,
      public memory: string,
      public chipset: string,
      public url: string,
      public coreClock: string,
      public quantity: number = 1
  ) {
      super(
          id,
          name,
          price
      );
  }

  check(items){
    return new Promise((resolve, reject) => {
        if(items.some(e => e instanceof GPU))
            reject('Već ste dodali grafičku karticu!');
        resolve();
    });
  }

  render(){
    return `
        <span> <strong>Memorija:</strong> ${this.memory}</span>
        <span> <strong>Clock:</strong> ${this.coreClock} Ghz</span>
    `;
  }

  renderCart(){
    return this.render();
  }
}

export class RAM extends Component {
  constructor(
      public id: number,
      public name: string,
      public price: number,
      public speed: string,
      public type: string,
      public cas: string,
      public url: string,
      public modules: string,
      public size: string,
      public ppg: string,
      public slots: string = (modules.split('x'))[0],
      public quantity: number = 1
  ) {
      super(
          id,
          name,
          price
      );
  }

  render(){
    return `
        <span> <strong>Brzina:</strong> ${this.speed}</span>
        <span> <strong>Veličina:</strong> ${this.size}</span>
        <span> <strong>Slotovi:</strong> ${this.slots}</span>
    `;
  }

  renderCart(){
    return this.render();
  }

  check(items){
    return new Promise((resolve, reject) => {
        if(items.some(e => e instanceof RAM))
            reject('Već ste dodali memoriju!');

        //if(items.some(e => e instanceof Motherboard).filter(e => e.socket != item.socket).length > 0)
            //reject('Socket procesora se ne poklapa sa matičnom pločom!');
        //if(items.some(e => e instanceof RAM))

        resolve();
    });
  }
}

export class Storage extends Component {
  constructor(
      public id: number,
      public name: string,
      public price: number,
      public series: string,
      public form: string,
      public type: string,
      public url: string,
      public capacity: string,
      public cache: string,
      public ppg: string,
      public quantity: number = 1
  ) {
      super(
          id,
          name,
          price
      );
  }

  check(items){
    return new Promise((resolve, reject) => {
        let num = items
            .filter(e => e instanceof Storage)
            .map(e => e.quantity)
            .reduce((total, acc) => total + acc, 0);
        if(num > 3)
            reject('Ne možete dodati više od 4 diska!');

        //if(items.some(e => e instanceof CPU).filter(e => e.socket != item.socket).length > 0)
            //reject('Socket procesora se ne poklapa sa matičnom pločom!');
        //if(items.some(e => e instanceof RAM))

        resolve();
    });
  }

  render(){
    return `
        <span> <strong>Form:</strong> ${this.form}</span>
        <span> <strong>Kapacitet:</strong> ${this.capacity}</span>
        <span> <strong>Cache:</strong> ${this.cache}</span>
    `;
  }

  renderCart(){
    return this.render();
  }
}

