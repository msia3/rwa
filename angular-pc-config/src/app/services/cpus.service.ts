import { Injectable } from '@angular/core';

import { Motherboard, CPU } from '../models/components';

import { getCPUs } from './pcpartpicker';
import { KursnaLista } from './kursnaLista.service';

@Injectable()
export class CPUsService {

  cpus: CPU[];

  constructor(private kursnaLista: KursnaLista){}

    get(items) {
        return new Promise((resolve) => {

            /*
            if(this.cpus.length > 0){
                console.log('Getting CPUs from local storage');
                return resolve(this.cpus);
            }
            */
            console.log('Getting CPUs from API');

            let sort = {};

            let mobos = items.filter(e => e instanceof Motherboard);
            if(mobos.length > 0 ){
                sort['socket'] = [mobos[0].socket];
            }

            getCPUs(sort,(cpus) => {
                this.cpus = cpus
                .map((cpu) => new CPU(
                    cpu.id, cpu.name, this.kursnaLista.konvertuj(cpu.price), cpu.speed, cpu.tdp, cpu.cores, cpu.url
                ));
                resolve(this.cpus);
            }, 1);
        });
    }
}
