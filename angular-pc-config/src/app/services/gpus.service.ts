import { Injectable } from '@angular/core';

import { GPU } from '../models/components';

import { getGPUs } from './pcpartpicker';
import { KursnaLista } from './kursnaLista.service';

@Injectable()
export class GPUService {

    gpus: GPU[];

    constructor(private kursnaLista: KursnaLista){}

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting GPU from API');

            let sort = {};

            getGPUs((gpus) => {
                this.gpus = gpus
                .map((gpu) => new GPU(
                    gpu.id, gpu.name, this.kursnaLista.konvertuj(gpu.price), gpu.series, gpu.memory, gpu.chipset, gpu.url, gpu.coreClock,
                ));
                resolve(this.gpus);
            }, 1);
        });
    }
}
