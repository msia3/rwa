import { Injectable } from '@angular/core';

import { Motherboard, RAM } from '../models/components';

import { getMemory } from './pcpartpicker';
import { KursnaLista } from './kursnaLista.service';

@Injectable()
export class MemoryService {

    rams: RAM[];

    constructor(private kursnaLista: KursnaLista){}

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting RAM from API');

            let sort = {};

            let mobos = items.filter(e => e instanceof Motherboard);
            if(mobos.length > 0 ){
                sort['price'] = '+';

                let per = parseInt(mobos[0].maxRam)/parseInt(mobos[0].ramSlots);
                sort['size'] = [mobos[0].maxRam + "GB (" + mobos[0].ramSlots + "x" + per + "GB)"];
            }

            getMemory(sort,(rams) => {
                this.rams = rams
                .map((ram) => new RAM(
                    ram.id, ram.name, this.kursnaLista.konvertuj(ram.price), ram.speed, ram.type, ram.cas, ram.url, ram.modules, ram.size, ram.ppg,
                ));
                resolve(this.rams);
            }, 1);
        });
    }
}
