import { Injectable } from '@angular/core';

import { Motherboard, CPU, RAM } from '../models/components';

import { getMotherboards } from './pcpartpicker';
import { KursnaLista } from './kursnaLista.service';

@Injectable()
export class MotherboardsService {

    motherboards: Motherboard[];

    constructor(private kursnaLista: KursnaLista){}

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting Mobos from API');

            let sort = {};

            let cpus = items.filter(e => e instanceof CPU);
            if(cpus.length > 0 ){
                sort['socket'] = [cpus[0].socket];
            }

            let ram = items.filter(e => e instanceof RAM);
            if(ram.length > 0 ){
                sort['size'] = [ram[0].size + "GB -" + ram[0].slots];
            }

            getMotherboards(sort,(mobos) => {
                this.motherboards = mobos
                .map((mobo) => new Motherboard(
                    mobo.id, mobo.name, mobo.socket, this.kursnaLista.konvertuj(mobo.price), mobo.formFactor, mobo.ramSlots, mobo.maxRAM, mobo.url
                ));
                resolve(this.motherboards);
            }, 1);
        });
    }
}
