import { Injectable } from '@angular/core';

@Injectable()
export class KursnaLista {

    API_KEY:string;
    API_URL:string;
    USD:number;

    /**
     * @param {string} api - API kljuc neophodan za koriscenje API-ja
     */
    constructor(){
        this.API_KEY = 'ad4195ec9396867b7a234ff1d29fe0b9';
        this.API_URL = `https://api.kursna-lista.info/${this.API_KEY}`;
        this.USD = null;

        this.init();
    }

    init(){
        return this.pretvori(1)
        .then((res) => this.USD = parseFloat(res));
    }

    /**
     * Metoda konvertuje unetu vrednost iz USD valute u RSD valutu
     * @param {number} value - Vrednost koju je potrebno konvertovati
     */
    pretvori(value){
        return fetch(this.API_URL + '/konvertor/usd/rsd/' + value)
        .then(res => res.json())
        .then((res) => {
            if(res.code == 0)
                return res.result.value;
            return 0;
        })
        .catch((error) => {
            console.log(error);
            return 0;
        });
    }

    /**
     * Metoda pretvara prosledjenu vrednost iz USD valute u RSD valutu
     * Razlika u odnosu na prethodne metode je sto koristi lokalni cache da ne bi stalno pozivali API
     * Ovim stedimo na vremenu koje je potrebno za obradu valute
     * @param {number} value - Vrednost koja se konvertuje
     */
    konvertuj(value = 1){
        /*
        return new Promise((resolve) => {
            if(this.USD == null){
                return this.pretvori(1)
                    .then((res) => this.USD = parseFloat(res))
                    .then(() => {
                        resolve(this.USD * value);
                    });
            }
            resolve(this.USD * value);
        });
        */
       value = value === null ? 0 : value;
       return parseFloat((this.USD * value).toFixed(2));
    }

}
