import { Injectable } from '@angular/core';

import { Storage} from '../models/components';

import { getStorage } from './pcpartpicker';
import { KursnaLista } from './kursnaLista.service';

@Injectable()
export class StorageService {

    type: string = 'SSD';
    storage: Storage[];

    constructor(private kursnaLista: KursnaLista){}

    setType(type: string){
      this.type = type;
    }

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting Storage from API');

            let sort = {};
            sort['type'] = this.type;

            getStorage(sort,(storage) => {
                this.storage = storage
                .map((item) => new Storage(
                    item.id, item.name, this.kursnaLista.konvertuj(item.price), item.series, item.form, item.type, item.url, item.capacityArray, item.cache, item.pricePerGB,
                ));
                resolve(this.storage);
            }, 1);
        });
    }
}
