import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../store/reducers';
import { getCartItems, getCartQuantity, getCartCost } from '../../store/reducers/cart.reducer';
import { Component as Comp } from '../../models/components';
import { Observable } from 'rxjs';
import { Formatter } from '../../helpers/formatter';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items$: Observable<Comp[]>;
  quantity$: Observable<number>;
  cost$: Observable<number>;

  constructor(private store$: Store<State>, private formatter: Formatter) {
    this.items$ = this.store$.pipe(select(getCartItems));
    this.quantity$ = this.store$.pipe(select(getCartQuantity));
    this.cost$ = this.store$.pipe(select(getCartCost));
  }

  ngOnInit() {
  }

}
