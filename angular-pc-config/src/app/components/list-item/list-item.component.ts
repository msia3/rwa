import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { addItem } from '../../store/actions';
import { State } from '../../store/reducers';
import { Component as Comp } from '../../models/components';
import { getCartItems } from '../../store/reducers/cart.reducer';

import { Formatter } from '../../helpers/formatter';

import notify from 'izitoast';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  @Input()
  public item: Comp;

  constructor(private store$: Store<State>, private formatter: Formatter) { }

  ngOnInit() {
  }

  public addToCart(item: Comp) {
    this.store$.pipe(take(1), select(getCartItems)).subscribe((items) => {
      item.check(items)
        .then(() => this.store$.dispatch(new addItem(item)))
        .catch((error) => {
        notify.error({
            'title': 'Greška',
            'message': error,
        });
      })
    }).unsubscribe();
  }
}
