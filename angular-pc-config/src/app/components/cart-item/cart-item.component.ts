import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { removeItem } from '../../store/actions';
import { State } from '../../store/reducers';
import { Component as Comp } from '../../models/components';

import { Formatter } from '../../helpers/formatter';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {

  @Input()
  public item: Comp;

  constructor(private store$: Store<State>, private formatter: Formatter) { }

  ngOnInit() {
  }

  removeFromCart(item: Comp) {
    this.store$.dispatch(new removeItem(item));
  }
}
