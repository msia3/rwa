import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../store/reducers';
import { getItemsList, getItemsLoading } from '../../store/reducers/list.reducer';
import { Component as Comp } from '../../models/components';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  items$: Observable<Comp[]>
  loading$: Observable<boolean>;

  constructor(private store$: Store<State>) {
    this.items$ = this.store$.pipe(select(getItemsList));
    this.loading$ = this.store$.pipe(select(getItemsLoading));
  }

  ngOnInit() {
  }

}
