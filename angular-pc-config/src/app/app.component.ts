import { Component, OnInit } from '@angular/core';
import * as pcpartpicker from './services/pcpartpicker';
import { Store } from '@ngrx/store';
import { State } from './store/reducers';
import {requestAPI } from './store/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private store$:Store<State>){

  }

  ngOnInit(){
    this.store$.dispatch(new requestAPI('mobo'));
  }

  public requestData(data: string){
    this.store$.dispatch(new requestAPI(data));
  }
}
