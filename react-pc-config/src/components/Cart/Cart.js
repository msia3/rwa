import React, { Component } from "react";
import Item from '../CartItem';
import { Formatter } from '../../helpers/formatter';


import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class Cart extends Component {

  constructor(props){
    super(props);

    this.formatter = new Formatter();
  }
  render() {
    return (
      <div className="col-md-4 order-md-2 mb-4">
        <h4 className="d-flex justify-content-between align-items-center mb-3">
          <span className="text-muted">Vaša konfiguracija</span>
          <span className="badge badge-secondary badge-pill items-count">{ this.props.quantity || 0 }</span>
        </h4>
        <ul className="list-group mb-3" id="cart-list">
          {this.renderList()}
          <li className="list-group-item d-flex justify-content-between">
            <span>Ukupno:</span>
            <strong id="total">{ this.props.cost > 0 ? this.formatter.ispisi(this.props.cost) : '--' }</strong>
          </li>
        </ul>
      </div>
    );
  }

  renderList() {
    if(!this.props.items) return;

    return this.props.items.map((item, i) => {
      return <Item item={item} key={i} id={i}/>;
    })
  }
}

function mapStateToProps(state){
  console.log(state);
  return {
    items: state.cart.items,
    quantity: state.cart.quantity,
    cost: state.cart.cost
  }
}

function MapDispatchToProps(dispatch){
  return bindActionCreators({

  }, dispatch);
}

export default connect(mapStateToProps, MapDispatchToProps)(Cart);
