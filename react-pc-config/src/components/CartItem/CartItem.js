import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import {removeFromCart} from '../../store/actions';


class CartItem extends Component {
  render() {
    return this.props.item.renderCart(this);
  }
}

function mapStateToProps(state){
  return state;
}

function MapDispatchToProps(dispatch){
  return bindActionCreators({
    removeFromCart: removeFromCart
  }, dispatch);
}

export default connect(mapStateToProps,MapDispatchToProps)(CartItem);
