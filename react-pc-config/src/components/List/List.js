import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

//Komponente
import Loader from '../Loader';
import Item from '../ListItem';

import {} from '../../store/actions';

class List extends Component {

  render() {
    return (
      <div className="col-md-8">
        {this.props.loading ? <Loader/> : ''}
        <ul className="list-group d-flex" id="component-list">
          {this.renderList()}
        </ul>
      </div>
    );
  }

  renderList() {
    return this.props.items.map((item, i) => {
      return <Item item={item} key={i} id={i}/>;
    })
  }
}

function mapStateToProps(state){
  return {
    items: state.list.items,
    loading: state.list.loading
  }
}

function MapDispatchToProps(dispatch){
  return bindActionCreators({

  }, dispatch);
}

export default connect(mapStateToProps, MapDispatchToProps)(List);
