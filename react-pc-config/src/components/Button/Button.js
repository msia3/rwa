import React, { Component } from "react";
import { Button, Tooltip } from 'reactstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { requestAPI } from '../../store/actions';

class Dugme extends React.Component {

  constructor(props){
    super(props);

    this.toggle = this.toggle.bind(this);

    //default state
    this.state = {
      tooltipOpen: false,
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render() {
    return (
      <span>
        <Button onClick={() => this.props.requestData(this.props.service)} id={'Tooltip-' + this.props.id} className="btn btn-lg btn-outline-primary mr-2" type="button" >
          <img src={this.props.img} />
        </Button>
        <Tooltip toggle={this.toggle} delay={{show:0, hide: 0}} placement="top" isOpen={this.state.tooltipOpen} target={'Tooltip-' + this.props.id} toggle={this.toggle}>{this.props.text}</Tooltip>
      </span>
    );
  }
}

function mapStateToProps(state){
  return {
    //service: state.service
  }
}

function MapDispatchToProps(dispatch){
  return bindActionCreators({
    requestData: requestAPI
  }, dispatch);
}


export default connect(mapStateToProps, MapDispatchToProps)(Dugme);
