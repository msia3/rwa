import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import {addToCart} from '../../store/actions';

class ListItem extends Component {
  render() {
    return this.props.item._renderElement(this);
  }
}

function MapDispatchToProps(dispatch){
  return bindActionCreators({
    addToCart: addToCart
  }, dispatch);
}

export default connect(null,MapDispatchToProps)(ListItem);