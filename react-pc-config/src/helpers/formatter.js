export class Formatter {
    constructor(){ }

    ispisi(value){
        var formatter = new Intl.NumberFormat('sr-RS', {
            style: 'currency',
            currency: 'RSD',
            minimumFractionDigits: 2,
        });

        return formatter.format(value);
    }
}