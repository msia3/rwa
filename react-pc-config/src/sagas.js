import { call, put, takeEvery, takeLatest, select } from 'redux-saga/effects';
import { REQUEST_API_DATA, receiveAPI, ADD_TO_CART, ADD_ITEM } from './store/actions/index';

//Notify
import notify from 'izitoast';

export const getCart = (state) => state.cart

export function* getApiData(action){
    let cart = yield select(getCart);
    const data = yield action.service.get(cart.items);
    yield put(receiveAPI(data));
}

function* checkItem(action){
    let cart = yield select(getCart);
    try {
        yield call(action.item.check, cart.items);

        yield put({type: ADD_ITEM, item: action.item});
    } catch (error){
        notify.error({
            'title': 'Greška',
            'message': error,
        });
    }
}

export function* mySaga(){
    yield takeLatest(REQUEST_API_DATA, getApiData);
    yield takeLatest(ADD_TO_CART, checkItem);
}