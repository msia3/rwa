import React from "react";
import { Formatter } from '../../helpers/formatter';
import { getCPUsocket } from "../pcpartpicker";


export class CPU {

    constructor(name, price, speed, tdp, cores, url){
        this.name = name;
        this.price = price;
        this.speed = speed;
        this.tdp = tdp;
        this.cores = cores;
        this.url = url;
        this.socket = null;

        this.quantity = 1;

        this.formatter = new Formatter();
    }

    check(items){
        return new Promise((resolve, reject) => {
            if(items.some(e => e instanceof CPU))
                reject('Već ste dodali procesor!');

            //if(items.some(e => e instanceof Motherboard).filter(e => e.socket != item.socket).length > 0)
                //reject('Socket procesora se ne poklapa sa matičnom pločom!');
            //if(items.some(e => e instanceof RAM))

            resolve();
        });
    }

    renderCart(el){

        getCPUsocket(this.url)
        .then((socket) => {
            this.socket = socket;
        })
        .catch(() => console.log('CPU socket crawler error!'));

        return (
            <li key={el.props.id} id="item-{el.props.id}" className="list-group-item">
                <div className="row justify-content-between">
                    <div className="col-10">
                        <span> {this.name} </span>
                    </div>
                    <div className="col-2">
                        <button type="button" onClick={()=>el.props.removeFromCart(this)} className="btn btn-outline-danger btn-sm remove-item"><i className="fas fa-minus"></i></button>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <small>
                            <span> <span className="font-weight-bold">Brzina:</span> {this.speed} GHz</span>
                            <span> <span className="font-weight-bold">Snaga:</span> {this.tdp} W</span>
                            <span> <span className="font-weight-bold">Jezgra:</span> {this.cores}</span>
                        </small>
                    </div>
                </div>
                <div className="row justify-content-between">
                    <div className="col-6">
                        <span className="badge badge-primary">{this.formatter.ispisi(this.price)}</span>
                    </div>
                    <div className="col-6 text-right">
                        <span>Količina:</span>
                        <span className="quantity">1</span>
                    </div>
                </div>
            </li>
        );
    }

    _renderElement(el){
        return (
            <li key={el.props.id} className="list-group-item">
                <div className="row align-items-center h-100">
                    <div className="col-1 max-auto text-center">
                        <a href={this.url} target="_blank"><i className="fas fa-search fa-sm mr-2"></i></a>
                    </div>
                    <div className="col-6 pl-0">
                        <div className="row">
                            <div className="col">
                                <span> {this.name} </span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <small>
                                    <span> <span className="font-weight-bold">Brzina:</span> {this.speed} GHz</span>
                                    <span> <span className="font-weight-bold">Snaga:</span> {this.tdp} W</span>
                                    <span> <span className="font-weight-bold">Jezgra:</span> {this.cores}</span>
                                </small>
                            </div>
                        </div>
                    </div>
                    <div className="col-3">
                        <span className="badge badge-primary">{this.formatter.ispisi(this.price)}</span>
                    </div>
                    <div className="col-2">
                        <button type="button" onClick={()=>el.props.addToCart(this)} className="btn btn-outline-success add-item"><i className="fas fa-plus"></i></button>
                    </div>
                </div>
            </li>
        );
    }
}