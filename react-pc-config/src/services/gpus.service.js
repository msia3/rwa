import { GPU } from './components/gpu';

import { getGPUs } from "./pcpartpicker";

export class GPUService {

    constructor(icon, kursnaLista){
        this.name = 'Grafičke kartice';
        this.gpus = [];
        this.icon = icon;

        this.kursnaLista = kursnaLista;
    }

    get(items) {
        return new Promise((resolve) => {

            /*
            if(this.cpus.length > 0){
                console.log('Getting CPUs from local storage');                
                return resolve(this.cpus);
            }
            */
            console.log('Getting GPU from API');

            let sort = {};

            getGPUs((gpus) => {
                this.gpus = gpus
                .map((gpu) => new GPU(
                    gpu.name, this.kursnaLista.konvertuj(gpu.price), gpu.series, gpu.memory, gpu.chipset, gpu.coreClock, gpu.url
                ));
                resolve(this.gpus);
            }, 1);
        });
    }

    check(items, item){
        return new Promise((resolve, reject) => {
            if(items.some(e => e instanceof GPU))
                reject('Već ste dodali grafičku karticu!');
            resolve();
        });
    }

    render(body){
        console.log('GPU Render');

        return new Promise((resolve) => {
            body.innerHTML = '';
            this.gpus.forEach((gpu) => {
                gpu._renderElement(body);
            });

            resolve();
        });
    }
}