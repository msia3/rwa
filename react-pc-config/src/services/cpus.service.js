import { CPU } from './components/cpu';
import { Motherboard } from './components/motherboard';

import { getCPUs } from "./pcpartpicker";

export class CPUsService {

    constructor(icon, kursnaLista){
        this.name = 'Procesori';
        this.cpus = [];
        this.icon = icon;

        this.kursnaLista = kursnaLista;
    }

    get(items) {
        return new Promise((resolve) => {

            /*
            if(this.cpus.length > 0){
                console.log('Getting CPUs from local storage');                
                return resolve(this.cpus);
            }
            */
            console.log('Getting CPUs from API');

            let sort = {};

            let mobos = items.filter(e => e instanceof Motherboard);
            if(mobos.length > 0 ){
                sort.socket = [mobos[0].socket];
            }

            getCPUs(sort,(cpus) => {
                this.cpus = cpus
                .map((cpu) => new CPU(
                    cpu.name, this.kursnaLista.konvertuj(cpu.price), cpu.speed, cpu.tdp, cpu.cores, cpu.url
                ));
                resolve(this.cpus);
            }, 1);
        });
    }

    check(items, item){
        return new Promise((resolve, reject) => {
            if(items.some(e => e instanceof CPU))
                reject('Već ste dodali procesor!');

            //if(items.some(e => e instanceof Motherboard).filter(e => e.socket != item.socket).length > 0)
                //reject('Socket procesora se ne poklapa sa matičnom pločom!');
            //if(items.some(e => e instanceof RAM))

            resolve();
        });
    }

    render(body){
        console.log('CPU Render');

        return new Promise((resolve) => {
            body.innerHTML = '';
            this.cpus.forEach((cpu) => {
                cpu._renderElement(body);
            });

            resolve();
        });
    }
}