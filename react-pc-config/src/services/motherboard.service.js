import { Motherboard } from './components/motherboard';
import { CPU } from './components/cpu';
import { RAM } from './components/memory';

import { getMotherboards } from "./pcpartpicker";

export class MotherboardsService {

    constructor(icon, kursnaLista){
        this.name = 'Matične ploče';
        this.motherboards = [];
        this.icon = icon;

        this.kursnaLista = kursnaLista;
    }

    get(items) {
        return new Promise((resolve) => {
            console.log('Getting Mobos from API');

            let sort = {};

            let cpus = items.filter(e => e instanceof CPU);
            if(cpus.length > 0 ){
                sort.socket = [cpus[0].socket];
            }

            let ram = items.filter(e => e instanceof RAM);
            if(ram.length > 0 ){                
                sort.size = [ram[0].size + "GB -" + ram[0].slots];
            }
            
            getMotherboards(sort,(mobos) => {
                this.motherboards = mobos
                .map((mobo) => new Motherboard(
                    mobo.name, mobo.socket, this.kursnaLista.konvertuj(mobo.price), mobo.formFactor, mobo.ramSlots, mobo.maxRAM, mobo.url
                ));
                resolve(this.motherboards);
            }, 1);
        });
    }

    check(items, item){
        return new Promise((resolve, reject) => {
            if(items.some(e => e instanceof Motherboard))
                reject('Već ste dodali matičnu ploču!');

            //if(items.some(e => e instanceof CPU).filter(e => e.socket != item.socket).length > 0)
                //reject('Socket procesora se ne poklapa sa matičnom pločom!');
            //if(items.some(e => e instanceof RAM))

            resolve();
        });
    }

    render(body){
        console.log('Mobos Render');

        return new Promise((resolve) => {
            body.innerHTML = '';
            this.motherboards.forEach((mobo) => {
                mobo._renderElement(body);
            });

            resolve();
        });
    }
}