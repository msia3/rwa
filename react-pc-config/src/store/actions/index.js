export const REQUEST_API_DATA = 'REQUEST_API_DATA';
export const RECEIVE_API_DATA = 'RECEIVE_API_DATA';
export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';

export function requestAPI(service){
    console.log(`Requesting data from service ${service.name}`);

    return {
        type: REQUEST_API_DATA,
        service: service,
        loading: true
    }
}

export function receiveAPI(data){
    return {
        type: RECEIVE_API_DATA,
        data: data
    }
}

export function addToCart(item){
    return {
        type: ADD_TO_CART,
        item: item
    }
}

export function removeFromCart(item){
    return {
        type: REMOVE_ITEM,
        item: item
    }
}