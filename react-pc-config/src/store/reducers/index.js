import {combineReducers} from 'redux';
import DugmeReducer from './dugme.reducer';
import ListReducer from './list.reducer';
import CartReducer from './cart.reducer';


const rootReducer = combineReducers({
    state: DugmeReducer,
    list: ListReducer,
    cart: CartReducer
})

export default rootReducer;