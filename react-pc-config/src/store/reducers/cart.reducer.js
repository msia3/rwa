import {ADD_TO_CART, ADD_ITEM, REMOVE_ITEM } from '../actions';


export default (state = {items: [], quantity: 0, cost: 0}, {type, item}) => {
    switch(type){
        case ADD_TO_CART:
            return state;
        break;
        case ADD_ITEM: {
            let index = state.items.findIndex((e, index) => e instanceof item.constructor);

            const _state = {
                ...state,
                quantity: state.quantity + 1,
                cost: state.cost + parseFloat(item.price)
            }

            if(index == -1){
                _state.items = [...state.items, item];
            } else {
                _state.items[index].quantity += 1;
            }

            return _state;
        }
        break;
        case REMOVE_ITEM: {
            let index = state.items.findIndex((e, index) => e instanceof item.constructor);

            const _state = {
                ...state,
                quantity: state.quantity - 1,
                cost: state.cost - item.price
            }
            _state.items[index].quantity -= 1;

            if(!_state.items[index].quantity){
                _state.items.splice(index, 1);
            }
            return _state;
        }
        break;
    }

    return state;
};