import { RECEIVE_API_DATA, REQUEST_API_DATA} from '../actions';

export default (state = {items: [], loading: false}, {type, data}) => {
    switch(type){
        case REQUEST_API_DATA:
            return {
                ...state,
                loading: true
            }
        break;
        case RECEIVE_API_DATA:
            return {
                items: data,
                loading: false
            }
        break;
    }
    return state;
};