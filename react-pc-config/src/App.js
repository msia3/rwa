import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';

//Kursna Lista
import {KursnaLista} from './services/kursnaLista.service';

//Servisi - komponente
import {MotherboardsService} from './services/motherboard.service';
import {CPUsService} from './services/cpus.service';
import {MemoryService} from './services/memory.service';
import {GPUService} from './services/gpus.service';
import {StorageService} from './services/storage.service';

//Akcije
import { requestAPI } from './store/actions';

//Komponente
import Dugme from './components/Button';
import List from './components/List';
import Cart from './components/Cart';

//Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

//Font Awesome
import fontawesome from '@fortawesome/fontawesome';
import solid from '@fortawesome/fontawesome-free-solid';

//Saga
import {getApiData} from './sagas';


class App extends Component {

  constructor(props){
    super(props);

    this.kursnaLista = new KursnaLista('ad4195ec9396867b7a234ff1d29fe0b9');

    this.services = [
      new MotherboardsService('https://png.icons8.com/dusk/50/000000/motherboard.png', this.kursnaLista),
      
      new CPUsService('https://png.icons8.com/dusk/50/000000/processor.png', this.kursnaLista),
      new MemoryService('https://png.icons8.com/dusk/50/000000/memory-slot.png', this.kursnaLista),
      new GPUService('https://png.icons8.com/dusk/50/000000/network-card.png', this.kursnaLista),
      new StorageService('https://png.icons8.com/dusk/50/000000/ssd.png', 'SSD', 'SSD', this.kursnaLista),
      new StorageService('https://png.icons8.com/dusk/50/000000/hdd.png', 'HDD', 'HDD', this.kursnaLista)
      
    ];
  }

  componentDidMount() {
    this.kursnaLista.init()
    .then(() => {
      this.props.requestAPI(this.services[0]);
    });
  }

  render() {
    return (
      <div className="container h-100" id="app">
        <div className="px-3 py-3 pt-md-5 pb-md-5 mx-auto text-center"><h1 className="display-4">PC Konfigurator</h1></div>
        <div className="row mb-4">
          <div className="col-md-8">
            <div className="btn-toolbar">
              {
                this.services.map((service, i) => {
                  return <Dugme key={i} img={service.icon} text={service.name} service={service} id={i} />; 
                })
              }
            </div>
          </div>
        </div>
        <div className="row mb-4">
          <List />
          <Cart />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {}
}

const MapDispatchToProps = {
  requestAPI
}


export default connect(mapStateToProps, MapDispatchToProps)(App);